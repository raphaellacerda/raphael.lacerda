import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import './App.css';
import TelaDetalheEpisodio from './components/telaDetalheEpisodio'
import PaginaInicial from './components/paginaInicial';
import ListaAvaliacoes from './components/ListaAvaliacoes';
import TodosEpisodios from './components/todosEpisodios';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <React.Fragment>
            <section className="screen">
              <Route path="/" exact component={ PaginaInicial }/>
              <Route path="/avaliacoes" component={ ListaAvaliacoes }/>
              <Route path="/detalhes/:id" component={ TelaDetalheEpisodio }/>
              <Route path="/todosEpisodios" component={ TodosEpisodios }/>

            </section>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}

export default App; 
