import React, { Component } from 'react'
import MeuBotao from './shared/meuBotao';

const TodosEpisodios = props => {
    const { listaEpisodios } = props.location.state
    return (
        <div>
            {listaEpisodios.todos.map(e =>
                <div>
                    <img src={e.thumbUrl} alt={e.nome}></img>
                    <p>{`${e.nome} - ${e.nota || 'Não avaliado'}`}</p>
                </div>
            )}
            <MeuBotao cor='azul' texto='Ord. Duração'/>
            <MeuBotao cor='azul' texto='Ord. Data de Estréia'/>
        
        </div>
    )
}

export default TodosEpisodios;