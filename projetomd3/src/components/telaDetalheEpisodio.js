import React, { Component } from 'react'
import EpisodiosApi from '../models/episodiosApi';

export default class TelaDetalheEpisodio extends Component {
    constructor(props) {
        super(props)
        this.episodiosApi = new EpisodiosApi()
        this.state = {
            detalhes: {}
        }
    }

    componentDidMount() {
        this.episodiosApi.buscarDetalhes(this.props.location.state.episodio.id)
            .then(detalhesDoServidor => {
                this.setState({
                    detalhes: detalhesDoServidor.data[0]
                })
            })
    }

    render() {
        const { detalhes } = this.state
        const { episodio } = this.props.location.state
        return <React.Fragment>
            <h1>{episodio.nome}</h1>
            <img src={episodio.thumbUrl} alt={episodio.nome}></img>
            <span>Temporada / Episódio: {episodio.temporadaEpisodio}</span>
            <span>Duração: {episodio.duracaoEmMin}</span>
            <span>Sinopse: {detalhes.sinopse}</span>
            <span>Data de Estréia: {`${new Date(detalhes.dataEstreia).toLocaleDateString()}`}</span>
            <span>Nota: {episodio.nota}</span>
            <span>Nota IMDB: {detalhes.notaImdb / 2}</span>
        </React.Fragment>
    }
}
// Nome
// Imagem
// Temporada / Episódio
// Duração
// Sinopse
// Data de estreia no formato “DD/MM/YYYY”
// Nota que você deu
// Nota do IMDB (convertido para escala de 1 a 5 que usamos)

