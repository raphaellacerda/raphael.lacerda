import React from 'react'
import './ListaAvaliacoes.css'
import MeuBotao from './shared/meuBotao';

const ListaAvaliacoes = props => {
  const { listaEpisodios } = props.location.state
  return listaEpisodios.avaliados.map(e =>
    <div className='card'>
      <img src={e.thumbUrl} alt={e.nome}></img>
      <p>{`${e.nome} - ${e.nota}`}</p>
      <MeuBotao cor="azul" link={`/detalhes/${e.id}`} dadosNavegacao={ { episodio: e } } texto="Detalhes"/>
    </div>
  )
}

export default ListaAvaliacoes

  //<li key={e.id}>{`${e.nome} - ${e.nota}`}</li>