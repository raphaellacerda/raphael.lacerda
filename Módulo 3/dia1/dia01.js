var hello = 'Hello'
function somar(a, b) {
    return a + b
}
//arrow functions
//var somarFn = (a,b) => a+b

var resultado = somar(1, 2)

console.log(resultado)

var circulo1 = { r: 12, tipoCalculo: 'A' }
var circulo2 = { r: 12, tipoCalculo: 'C' }
function calcularCirculo(circulo) {
    var pi = 3.14
    if (circulo.tipoCalculo === 'A') {
        return pi * Math.pow(circulo.r, 2)
    }
    if (circulo.tipoCalculo === 'C') {
        return pi * (circulo.r * 2)
    }
}

function naoBissexto(ano) {
    var d4 = ano % 4
    var d100 = ano % 100
    if (d4 === 0 && d100 !== 0) {
        return true
    }
    return false
}