
var pokeApi = new PokeApi()
pokeApi.buscar(144)
    .then(pokemonServidor => {
        var poke = new Pokemon(pokemonServidor)
        renderizaPokemonNaTela(poke)
    } )

function renderizaPokemonNaTela(pokemon) {
    var dadosPokemon = document.getElementById('dadosPokemon')
    var nome = dadosPokemon.querySelector('.nome')
    nome.innerText = pokemon.nome
    var imgPokemon = dadosPokemon.querySelector('.thumb')
    imgPokemon.src = pokemon.thumbUrl
}