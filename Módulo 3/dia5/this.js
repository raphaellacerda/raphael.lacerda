
let executarIssoQuandoCarregar = function () {
    let h1titulo = document.getElementById('titulo')
    h1titulo.innerText = 'Lesgou'
}
document.addEventListener('DOMContentLoaded', executarIssoQuandoCarregar)

var luke = {
    nome: "Luke Skywalker",
    idade: 23,
    imprimirInformacoes: function(forca , irma) {
        return `${ this.nome } ${ this.idade } ${ forca } ${ irma }`
    }
}

let outraFunc = luke.imprimirInformacoes

console.log(luke.imprimirInformacoes())
console.log(outraFunc())
window.nome = 'global'
console.log(outraFunc())
console.log(outraFunc.call(luke, 45, 'Princess Leia'))
console.log(outraFunc.apply(luke, [45, 'Princess Leia']))

var outraFuncComBind = luke.imprimirInformacoes.bind( luke )
console.log ( `com bind: ${ outraFuncComBind() } `)

window.setTimeout( function() {
    console.log( `dentro do setTimeout: ${ this.nome }`)
}, 2000)