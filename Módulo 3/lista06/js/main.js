Array.prototype.invalidas = function () {
    
    const invalidas = this.filter(serie => {
        const campoInvalido = Object.values( serie ).some( v => v === null || typeof v === 'undefined')
        const anoInvalido = serie.anoEstreia > new Date().getFullYear()
        return campoInvalido || anoInvalido
    })
    return `Séries inválidas: ${ invalidas.map(serie => serie.titulo).join( ' - ' )}`
    
  };

//ex2: series.map( s => s.numeroEpisodios).reduce( (acc , elem) => acc + elem, 0) / series.lenght
//ex3: series.sort( ( a, b) => a.titulo.localeCompare ( a + b))
