class Pokemon {
  constructor (objVindoDaApi) {
    this.id = objVindoDaApi.id;
    this.nome = objVindoDaApi.name;
    this.thumbUrl = objVindoDaApi.sprites.front_default;
    this._altura = objVindoDaApi.height;
    this._peso = objVindoDaApi.weight;
    this.tipos = [];
    this.estatisticas = [];

    //preenche os tipos do pokemon
    this.tipos = objVindoDaApi.types.map( element => element.type.name ) 

    //preenche as estatisticas do pokemon
    this.estatisticas = objVindoDaApi.stats.map( estatElemento => {
      let estatistica = " " + estatElemento.base_stat + "% of ";
      estatistica += estatElemento.stat.name;
      return estatistica
    }) 
  }

  //pokemon.altura
  get altura(){
    //transformar altura de dezenas de cm para cm
    return this._altura * 10
  }

  get peso(){
    //transforma altura de hectogramas para kg
    return this._peso / 10
  }

  get alturaEmCm() {
    return `Altura: ${ this.altura } cm`
  }

  get pesoEmKg(){
    return `Peso: ${ this.peso } kg`
  }
}

