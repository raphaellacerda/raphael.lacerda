package br.com.dbc.lotr;

import br.com.dbc.lotr.entity.ElfoTabelao;
import br.com.dbc.lotr.entity.HibernateUtil;
import br.com.dbc.lotr.entity.HobbitTabelao;
import br.com.dbc.lotr.entity.Usuario;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            Usuario usuario = new Usuario();
            usuario.setNome("Antônio");
            usuario.setApelido("Antônio");
            usuario.setCpf(123l);
            usuario.setSenha("456");

            session.save(usuario);
            
            ElfoTabelao elfoTabelao = new ElfoTabelao();
            elfoTabelao.setDanoElfo(100d);
            elfoTabelao.setNome("Legolas");
            session.save(elfoTabelao);
            
            HobbitTabelao hobbitTabelao = new HobbitTabelao();
            hobbitTabelao.setDanoHobbit(10d);
            hobbitTabelao.setNome("Frodo Bonceiro");
            session.save(hobbitTabelao);

            transaction.commit();
        } catch (Exception ex) {
            if(transaction != null)
                transaction.rollback();
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            System.exit(1);
        } finally {
            if(session != null)
                session.close();
            System.exit(0);
        }

    }

}
