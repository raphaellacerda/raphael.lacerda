package br.com.dbc.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {
        Connection conn = Connector.connect();
        boolean executed = false;
        try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'USUARIO'")
                    .executeQuery();
            if (!rs.next()) {
                conn.prepareStatement("CREATE TABLE USUARIO (\n"
                        + "  ID INTEGER NOT NULL PRIMARY KEY,\n"
                        + "  nome VARCHAR(100) NOT NULL,\n"
                        + "  apelido VARCHAR(100) NOT NULL,\n"
                        + "  senha VARCHAR(15) NOT NULL,\n"
                        + "  email VARCHAR(100) NOT NULL\n"
                        + ")").execute();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName())
                    .log(Level.SEVERE, "Erro de consulta no Main", ex);
        }
        System.out.println("Executado: " + executed);
    }

}
