package br.com.dbccompany.Entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "ID_PESSOA")
public class Corretor extends Pessoa {

	@Column(name = "CARGO")
	private String cargo;

	@Column(name = "COMISSAO")
	private long comissao;

	@OneToMany(mappedBy = "corretor")
	private List<ServicoContratado> ServicosContratados;

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public long getComissao() {
		return comissao;
	}

	public void setComissao(long comissao) {
		this.comissao = comissao;
	}

}
