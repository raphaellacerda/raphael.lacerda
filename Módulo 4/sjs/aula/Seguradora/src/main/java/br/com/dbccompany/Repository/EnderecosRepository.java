package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Enderecos;

public interface EnderecosRepository<E extends Enderecos> extends CrudRepository<Enderecos, Long> {

	E findByLogradouro(String logradouro);

}
