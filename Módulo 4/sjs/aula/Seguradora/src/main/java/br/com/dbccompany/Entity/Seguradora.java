package br.com.dbccompany.Entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Seguradora {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;

	@Column(name = "NOME")
	private String nome;

	@Column(name = "CNPJ")
	private String cnpj;

	@OneToMany(mappedBy = "seguradora")
	private List<ServicoContratado> servicosContratados;

	@OneToOne(mappedBy = "seguradora")
	private EnderecoSeguradora enderecoEnderecoSeguradora;

	@ManyToMany(mappedBy = "seguradora")
	private List<Servicos> servicos;

	@ManyToOne
	@JoinColumn(name = "ID_ENDERECO_ENDERECOS_SEGURADORA")
	private EnderecoSeguradora enderecoSeguradora;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public List<ServicoContratado> getServicosContratados() {
		return servicosContratados;
	}

	public void setServicosContratados(List<ServicoContratado> servicosContratados) {
		this.servicosContratados = servicosContratados;
	}

	public EnderecoSeguradora getEnderecoSeguradora() {
		return enderecoSeguradora;
	}

	public void setEnderecoSeguradora(EnderecoSeguradora enderecoSeguradora) {
		this.enderecoSeguradora = enderecoSeguradora;
	}

	public List<Servicos> getServicos() {
		return servicos;
	}

	public void setServicos(List<Servicos> servicos) {
		this.servicos = servicos;
	}

	public EnderecoSeguradora getEnderecoEnderecoSeguradora() {
		return enderecoEnderecoSeguradora;
	}

	public void setEnderecoEnderecoSeguradora(EnderecoSeguradora enderecoEnderecoSeguradora) {
		this.enderecoEnderecoSeguradora = enderecoEnderecoSeguradora;
	}

}
