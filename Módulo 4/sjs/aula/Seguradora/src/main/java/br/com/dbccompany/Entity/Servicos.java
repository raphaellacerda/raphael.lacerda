package br.com.dbccompany.Entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Servicos {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;

	@Column(name = "NOME")
	private String nome;

	@Column(name = "DESCRICAO")
	private String descricao;

	@Column(name = "VALOR_PADRAO")
	private long valorPadrao;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "SERVICO_SEGURADORA", joinColumns = { @JoinColumn(name = "ID_SERVICO") }, inverseJoinColumns = {
			@JoinColumn(name = "ID_SEGURADORA") })
	private List<Seguradora> seguradora;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public long getValorPadrao() {
		return valorPadrao;
	}

	public void setValorPadrao(long valorPadrao) {
		this.valorPadrao = valorPadrao;
	}

	public List<Seguradora> getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(List<Seguradora> seguradora) {
		this.seguradora = seguradora;
	}

}
