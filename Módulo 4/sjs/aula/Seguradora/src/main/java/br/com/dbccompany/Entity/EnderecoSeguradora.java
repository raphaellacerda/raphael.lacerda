package br.com.dbccompany.Entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "ID_ENDERECO")
public class EnderecoSeguradora extends Enderecos {

	@OneToOne
	@JoinColumn(name = "ID_SEGURADORA")
	private Seguradora seguradora;

	@OneToMany(mappedBy = "enderecoEnderecoSeguradora")
	private List<Seguradora> listaSeguradora;
	
	public Seguradora getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(Seguradora seguradora) {
		this.seguradora = seguradora;
	}

}
