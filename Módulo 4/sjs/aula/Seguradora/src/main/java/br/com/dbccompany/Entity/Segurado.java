package br.com.dbccompany.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "ID_PESSOA")
public class Segurado extends Pessoa {

	@Column(name = "QTD_SERVICOS")
	private long qtdServicos;

	public long getQtdServicos() {
		return qtdServicos;
	}

	public void setQtdServicos(long qtdServicos) {
		this.qtdServicos = qtdServicos;
	}

}
