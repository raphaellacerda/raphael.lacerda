package br.com.dbc.jdbc;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Main {

    public static void main(String[] args) {
        Connection conn = Connector.connect();
        
        try {
            //Inserir na tabela da DB:
            PreparedStatement pst = conn.prepareStatement("insert into usuario(id, nome, apelido, senha, cpf) "
                    + "values (usuario_seq.nextval, ? ,?, ?, ?)");
            pst.setString(1, "Jones");
            pst.setString(2, "Jonesy");
            pst.setString(3, "123");
            pst.setInt(4, 123456);
            pst.executeUpdate();
            
            //Selecionar elemento da tabela
            ResultSet rs = conn.prepareStatement("select * from usuario").executeQuery();
                while(rs.next()){
                    System.out.println(String.format("ID do usuario: %s", rs.getInt("id")));
                    System.out.println(String.format("Nome do usuario: %s", rs.getString("nome")));
                    System.out.println(String.format("Apelido do usuario: %s", rs.getString("apelido")));
                    System.out.println(String.format("Senha do usuario: %s", rs.getString("senha")));
                    System.out.println(String.format("CPF do usuario: %s", rs.getInt("cpf")));
                }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro na consulta do Main", ex);
        }
        
    }
}
