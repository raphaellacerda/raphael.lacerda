
import java.util.ArrayList;
import java.util.HashMap;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElvenArmyTest {
    @Test
    public void enlistGreenElfToArmy(){
        ElvenArmy elvenArmy = new ElvenArmy();
        GreenElf greenElf = new GreenElf("Biroliro");
        elvenArmy.enlistElf(greenElf);
        assertEquals(greenElf, elvenArmy.getElf("Biroliro"));
    }
    
    @Test
    public void enlistNightElfToArmy(){
        ElvenArmy elvenArmy = new ElvenArmy();
        NightElf nightElf = new NightElf("Sylvannas");
        elvenArmy.enlistElf(nightElf);
        assertEquals(nightElf, elvenArmy.getElf("Sylvannas"));
    }
    
    @Test
    public void checkIfEnlistDontEnlistNormalElf(){
         ElvenArmy elvenArmy = new ElvenArmy();
         Elf elf = new Elf("Legolas");
         elvenArmy.enlistElf(elf);
         assertNull(elvenArmy.getElf("Legolas"));
    }
    
    @Test
    public void checkIfEnlistDontEnlisLightElf(){
         ElvenArmy elvenArmy = new ElvenArmy();
         LightElf lightElf = new LightElf("Celebrimbor");
         elvenArmy.enlistElf(lightElf);
         assertNull(elvenArmy.getElf("Celebrimbor"));
    }
    
    @Test
    public void enlistDamagedElf(){
    
    }
    
    @Test
    public void checkNEW_CHARElfIfEnlisted(){
    
    }
    
    @Test
    public void add5NEW_CHARand5DAMAGED(){
        
    }
}
