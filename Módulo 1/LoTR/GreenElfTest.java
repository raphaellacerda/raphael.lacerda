

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GreenElfTest {
    @Test
    public void greenElfMustGain2Xp(){
        GreenElf greenElf = new GreenElf("Blizzard");
        Dwarf dwarf = new Dwarf("Sindri");
        greenElf.shootArrow(dwarf);
        assertEquals(2, greenElf.getExp());
    }
    
    @Test
    public void greenElfMustGain4Xp(){
        GreenElf greenElf = new GreenElf("Blizzard");
        Dwarf dwarf = new Dwarf("Sindri");
        greenElf.shootArrow(dwarf);
        greenElf.shootArrow(dwarf);
        assertEquals(4, greenElf.getExp());
    }
    
    @Test
    public void greenElfMustNotGainXpWithoutArrows(){
        GreenElf greenElf = new GreenElf("Blizzard");
        Dwarf dwarf = new Dwarf("Sindri");
        greenElf.shootArrow(dwarf);
        greenElf.shootArrow(dwarf);
        greenElf.shootArrow(dwarf);
        assertEquals(4, greenElf.getExp());
    }
    
    @Test
    public void greenElfReceiveOnlySpecialItem(){
        GreenElf greenElf = new GreenElf("Tempest");
        greenElf.receiveItem(new Item("Valyrian Steel Sword", 1));
        greenElf.receiveItem(new Item("Glass Bow", 1));
        greenElf.receiveItem(new Item("Glass Arrow", 1));
        Inventory result = greenElf.getInventory();
        assertEquals("Valyrian Steel Sword", result.getDescription(3));
        assertEquals("Glass Bow", result.getDescription(4));
        assertEquals("Glass Arrow", result.getDescription(5));
    }
    
    @Test
    public void greenElfReceiveOtherlItem(){
        GreenElf greenElf = new GreenElf("Tempest");
        greenElf.receiveItem(new Item("Sword", 1));
        greenElf.receiveItem(new Item("Bow", 1));
        greenElf.receiveItem(new Item("Arrow", 1));
        Inventory result = greenElf.getInventory();
        assertNull(result.getItem(3));
        }
    
    @Test
    public void greenElfRemoveOnlySpecialItem(){
        GreenElf greenElf = new GreenElf("Tempest");
        greenElf.loseItem(greenElf.getInventory().getItem(2));
        greenElf.loseItem(greenElf.getInventory().getItem(1));
        greenElf.loseItem(greenElf.getInventory().getItem(0));
        Inventory result = greenElf.getInventory();
        assertNull(result.getItem(0));
        }
}