public class LightElf extends Elf{    

    private boolean galvSwordEquipped = false;
    private int healthModifier = 0;
    {
        experience = 0;
        this.backpack = new Inventory(10);
        health = 100.0;
    }

    public LightElf(String name){
        super(name);
        super.equipElf();
        this.equipLightElf();
    } 

    private void equipLightElf(){
        this.backpack.addItem(new Item("Galvorn Sword", 1));
    }

    public void equipSword(){
        this.galvSwordEquipped = true;
    }

    public boolean swordOnHand(){
        return galvSwordEquipped;
    }

    public void swordStrike(Dwarf dwarf){
        boolean heal;
        if(galvSwordEquipped && this.canLoseHealth()){
            healthModifier ++;            
            this.experience ++;
            dwarf.damageHealth();
            heal = (healthModifier % 2) == 0 ? true : false;
            if(heal){
                this.health += 10.0;
            }else{
                this.health -= 21.0;
                this.status = Status.DAMAGED;
                if (this.health <= 0){
                    this.status = Status.DEAD;
                }
            }
        }
    }
}