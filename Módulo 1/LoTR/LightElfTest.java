
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LightElfTest {
    @Test
    public void lightElfStartsWithGalvornSword(){
        LightElf elf = new LightElf("Celebrimbor");
        Item result = elf.backpack.getItem(2);
        assertEquals("Galvorn Sword", result.getName());
    }

    @Test
    public void lightElfCantStrikeWithoutSwordEquipped(){
        LightElf elf = new LightElf("Celebrimbor");
        Dwarf dwarf = new Dwarf("Torvin");
        elf.swordStrike(dwarf);
        assertEquals(110, dwarf.getHealth(), 1e-9);
    }

    @Test
    public void lightElfEquipsSword(){
        LightElf elf = new LightElf("Celebrimbor");
        elf.equipSword();
        assertTrue(elf.swordOnHand());
    }

    @Test
    public void lightElfStrikeDwarfFor10HP(){
        LightElf elf = new LightElf("Celebrimbor");
        Dwarf dwarf = new Dwarf("Torvin");
        elf.equipSword();
        elf.swordStrike(dwarf);
        assertEquals(100, dwarf.getHealth(), 1e-9);
    }

    @Test
    public void lightElfLosesHealthOnFirstStrike(){
        LightElf elf = new LightElf("Celebrimbor");
        Dwarf dwarf = new Dwarf("Torvin");
        elf.equipSword();
        elf.swordStrike(dwarf);
        assertEquals(79, elf.getHealth(), 1e-9);
    }

    @Test
    public void lightElfHeals10HPOnSecondStrike(){
        LightElf elf = new LightElf("Celebrimbor");
        Dwarf dwarf = new Dwarf("Torvin");
        elf.equipSword();
        elf.swordStrike(dwarf);
        elf.swordStrike(dwarf);
        assertEquals(89, elf.getHealth(), 1e-9);
    }

    @Test
    public void lightElfStatusDEADFromSwordStrikeWithMoreThanOneDwarf(){
        LightElf elf = new LightElf("Celebrimbor");
        Dwarf dwarfA = new Dwarf("Torvin");
        Dwarf dwarfB = new Dwarf("Brok");
        elf.equipSword();
        elf.swordStrike(dwarfA);
        elf.swordStrike(dwarfA);
        elf.swordStrike(dwarfA);
        elf.swordStrike(dwarfA);
        elf.swordStrike(dwarfA);
        elf.swordStrike(dwarfA);
        elf.swordStrike(dwarfA);
        elf.swordStrike(dwarfA);
        elf.swordStrike(dwarfA);
        elf.swordStrike(dwarfB);
        elf.swordStrike(dwarfB);
        elf.swordStrike(dwarfB);
        elf.swordStrike(dwarfB);
        elf.swordStrike(dwarfB);
        elf.swordStrike(dwarfB);
        elf.swordStrike(dwarfB);
        elf.swordStrike(dwarfB);
        elf.swordStrike(dwarfB);
        assertEquals(Status.DEAD, elf.getStatus());
    }

    @Test
    public void lightElfMustGainXpFromSwordStrike(){
        LightElf elf = new LightElf("Celebrimbor");
        Dwarf dwarf = new Dwarf("Torvin");
        elf.equipSword();
        elf.swordStrike(dwarf);;
        assertEquals(1, elf.getExp());
    }

    
}