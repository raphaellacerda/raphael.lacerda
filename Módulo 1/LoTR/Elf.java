public class Elf extends Character{

    private static int elfCounter = 0;

    {
        experience = 0;
        this.backpack = new Inventory(10);
        health = 100.0;
        this.elfCount();
    }

    public Elf(String name) {
        super(name);
        super.equipElf();
    }

    protected boolean canLoseHealth(){
        return this.health > 0 && this.status != Status.DEAD ? true : false;
    }

    public String characterOverall(){
        return this.name;
    }

    public static int totalElfCreated(){
        return elfCounter;
    }

    public static void elfCount(){
        elfCounter ++;
    }
}