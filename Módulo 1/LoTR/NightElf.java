public class NightElf extends Elf{

    {
        experience = 0;
        this.backpack = new Inventory(10);
        health = 100.0;
    }

    public NightElf(String name){
        super(name);
        super.equipElf();
    }
    
    private void gainXp(){
        this.experience += 3;
    }
    
    private boolean canShootArrow(){
        Item arrow = backpack.getItem(1);
        return arrow.getQty() > 0;
    }
    
    public void shootArrow(Dwarf dwarf){
        Item arrow = this.backpack.getItem(1);
        int totalArrows = arrow.getQty();
        if (canShootArrow()){    
            arrow.setQty(totalArrows - 1);
            this.gainXp();
            dwarf.damageHealth();
            this.health -= 15.0;
            this.status = Status.DAMAGED;
            if (this.health <= 0){
                this.status = Status.DEAD;
            }
        }
    }
}