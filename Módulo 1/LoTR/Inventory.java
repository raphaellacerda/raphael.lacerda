import java.util.ArrayList;

public class Inventory{
    private ArrayList<Item> items;
    private int actualPosition = 0, lastPosition = 0;
    
    public Inventory(int invSize){
        this.items = new ArrayList(invSize);
    }

    public String getDescription(int position){
        return this.items.get(position).getName();
    }

    public int getInventorySize(){
        return this.items.size();
    }

    public Item getItem(int position){
        if (position >= this.items.size()) {
            return null;
        }
        return this.items.get(position);
    }

    public void removeItem(int position) {
        this.items.remove(position);
    }

    public void addItem(Item item){
        this.items.add(item);
    }

    public ArrayList<Item> getItemList(){
        return this.items;
    }
    
    
    public String getItemsDescription(){
        StringBuilder invDesc = new StringBuilder();
        for (int i = 0; i < this.items.size(); i ++){
            Item item = this.items.get(i);
            if (item != null){
                String description = item.getName();
                invDesc.append(description);
                boolean mustGetComa = i < this.items.size() - 1;
                if (mustGetComa) {
                    invDesc.append(",");
                }
            }
        }

        return invDesc.toString();

    }

    public Item getHigherItemQty(){
        int biggestQty = 0, index = 0;
        for (int i = 0; i < items.size(); i++){
            if (items.get(i) != null){
                int qty = items.get(i).getQty();
                if(biggestQty < qty){
                    biggestQty = qty;
                    index = i;
                }                
            }
        }

        return this.items.size() > 0 ? this.items.get(index) : null;

    }
    
    public Item findItem(String name){
        for (Item neededItem : this.items){
            boolean itemFound = neededItem.getName().equals(name);
            if (itemFound){
                return neededItem;
            }
        }
        return null;
    }
    
    public ArrayList<Item> invertItems(){
        ArrayList<Item> invertedInventory = new ArrayList<>(this.items.size());
        for (int i = this.items.size() -1; i >= 0; i--){
            invertedInventory.add(this.items.get(i));
        }
        return invertedInventory;
    }
    
    public void sortItems(){
        for(int i = 0; i < this.items.size(); i++){
            for(int j = 0; j < this.items.size() - 1; j++){
                Item actualItem = this.items.get(j);
                Item nextItem = this.items.get(j + 1);
                boolean changePosition = actualItem.getQty() > nextItem.getQty();
                if (changePosition){
                    Item swappedItem = actualItem;
                    this.items.set(j, nextItem);
                    this.items.set(j + 1, swappedItem);
                }
            }
        }
    }
}
