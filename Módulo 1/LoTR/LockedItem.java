public class LockedItem extends Item{
    
    
    public LockedItem(String name, int qty){
        super(name, qty);
    }

    public void setQty(int quantity){
        this.quantity = quantity;
    }
    
    public void setName(String name){
        this.name = name;
    }
}