

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NightElfTest{
    
    @Test
    public void nightElfLosesHPOnShootArrow(){
        NightElf nElf = new NightElf("Warden");
        Dwarf dwarf = new Dwarf("Brok");
        nElf.shootArrow(dwarf);
        assertEquals(85.0, nElf.getHealth(), .00001);
    }
    
    @Test
    public void checkingInitialAmountArrows2(){
        NightElf anotherElf = new NightElf("Sylvannas");
        assertEquals(2, anotherElf.getArrow().getQty());
    }
    
    @Test
    public void nightElfGains3XPShootingArrow(){
        NightElf nElf = new NightElf("Warden");
        Dwarf dwarf = new Dwarf("Brok");
        nElf.shootArrow(dwarf);
        assertEquals(3, nElf.getExp(), .00001);
    }
    
    @Test
    public void nightElfDiesFromShootingArrow(){
        NightElf nElf = new NightElf("Warden");
        Dwarf dwarf = new Dwarf("Brok");
        nElf.backpack.removeItem(1);
        nElf.backpack.addItem(new Item("Arrow", 10));        
        nElf.shootArrow(dwarf);
        nElf.shootArrow(dwarf);
        nElf.shootArrow(dwarf);
        nElf.shootArrow(dwarf);
        nElf.shootArrow(dwarf);
        nElf.shootArrow(dwarf);
        nElf.shootArrow(dwarf);
        assertEquals(Status.DEAD, nElf.getStatus());
    }
}
