public abstract class Character {

    protected String name;
    protected Inventory backpack;
    protected Status status;
    protected double health;
    protected int experience;

    {
        status = Status.NEW_CHAR;
    }

    protected Character(String name){
        this.name = name;
    }   

    protected String getName(){
        return this.name;
    }

    protected void setName(String name){
        this.name = name;
    }

    protected Status getStatus(){
        return this.status;
    }

    protected Inventory getInventory(){
        return this.backpack;
    }

    protected double getHealth(){
        return this.health;
    }

    protected void receiveItem(Item item){
        this.backpack.addItem(item);
    }

    protected void equipElf(){
        this.backpack.addItem(new Item("Bow", 1));
        this.backpack.addItem(new Item("Arrow", 2));
    }

    protected void loseItem(Item item){
        Item foundItem = this.backpack.findItem(item.getName());
        for(int i = 0; i < this.backpack.getInventorySize(); i++){
            Item checkItem = this.backpack.getItem(i);
            if(checkItem == foundItem){
                this.backpack.removeItem(i);
            }
        }
    }

    private boolean canShootArrow(){
        Item arrow = backpack.getItem(1);
        return arrow.getQty() > 0;
    }

    protected void shootArrow(Dwarf dwarf){
        Item arrow = this.backpack.getItem(1);
        int totalArrows = arrow.getQty();
        if (canShootArrow()){    
            arrow.setQty(totalArrows - 1);
            this.gainXp();
            dwarf.damageHealth();
        }
    }

    protected int getExp(){
        return this.experience;
    }

    protected Item getArrow(){
        return this.backpack.getItem(1);
    }

    private void gainXp(){
        this.experience++;
    }
    
    public abstract String characterOverall();
    
}