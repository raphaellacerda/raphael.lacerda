import java.util.ArrayList;
import java.util.Random;

public class Dice6 {

    private ArrayList<Integer> dice = new ArrayList<>(6);

    {
        for(int i = 1; i <= 6; i++){
            this.dice.add(i);
        }
    }

    public int throwDice(){
        Random randomNumber = new Random();
        int number = randomNumber.nextInt(dice.size() -1) + 1;
        return number;
    }

    public ArrayList checkDice(){
        return dice;    
    }
}