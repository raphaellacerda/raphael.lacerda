import java.util.ArrayList;
import java.util.Arrays;

public class GreenElf extends Elf{
    
    private final ArrayList<String> validItemDescription = new ArrayList<>(
        Arrays.asList(
        "Valyrian Steel Sword",
        "Glass Bow",
        "Glass Arrow"
        )
    );
    
    {
        experience = 0;
        this.backpack = new Inventory(10);
        health = 100.0;
    }

    public GreenElf(String name){
        super(name);
        this.equipGreenElf();
    }

    private void equipGreenElf(){
        Item valyrianSteelSword = new Item("Valyrian Steel Sword", 1);
        Item glassBow = new Item("Glass Bow", 1);
        Item glassArrow = new Item("Glass Arrow", 2);
        this.backpack.addItem(valyrianSteelSword);
        this.backpack.addItem(glassBow);
        this.backpack.addItem(glassArrow);
    }

    private void gainXp(){
        this.experience += 2;
    }

    private boolean canShootArrow(){
        Item arrow = backpack.getItem(2);
        return arrow.getQty() > 0;
    }

    public void shootArrow(Dwarf dwarf){
        Item arrow = this.backpack.getItem(2);
        int totalArrows = arrow.getQty();
        if (canShootArrow()){    
            arrow.setQty(totalArrows - 1);
            this.gainXp();
            dwarf.damageHealth();
        }
    }

    public void receiveItem(Item item){
        boolean validName = validItemDescription.contains(item.getName());
        if (validName){
            this.backpack.addItem(item);
        }
        // if(item.getName() == "Valyrian Steel Sword" || item.getName() == "Glass Bow" || item.getName() == "Glass Arrow"){    
            // this.backpack.addItem(item);
        // }
    }

    public void loseItem(Item item){
        boolean validName = validItemDescription.contains(item.getName());
        Item foundItem = this.backpack.findItem(item.getName());
        for(int i = 0; i < this.backpack.getInventorySize(); i++){
            if(validName){
                Item checkItem = this.backpack.getItem(i);
                if(checkItem == foundItem){
                    this.backpack.removeItem(i);
                }
            }
        }
    }
}