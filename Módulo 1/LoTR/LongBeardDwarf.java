public class LongBeardDwarf extends Dwarf{

    private Dice6 dice = new Dice6();

    public LongBeardDwarf(String name){
        super(name);
    }

    private boolean evadeAttack(){
        int firstNumber = 0;
        int secondNumber = 0;
        while (firstNumber == secondNumber){
            firstNumber = dice.throwDice();
            secondNumber = dice.throwDice();
        }
        if(firstNumber != secondNumber){
            int evadeThrow = dice.throwDice();
            return evadeThrow == firstNumber || evadeThrow == secondNumber ? true : false;
        }
        return false;

    }

    public void damageHealth(){        
        if (evadeAttack() == false){
            if (super.canLoseHealth()){
                this.health -= this.shieldEquipped == true ? 5.0 : 10.0;
                this.status = Status.DAMAGED;
                if (this.health <= 0){
                    this.status = Status.DEAD;
                }
            }
        }
    }
}