public class Dwarf extends Character{

    protected boolean shieldEquipped = false;

    {
        health = 110.0;
        backpack = new Inventory(10);
        backpack.addItem(new Item("Shield", 1));
    }

    public Dwarf(String name){
        super(name);
    }

    public void equipShield(){
        this.shieldEquipped = true;
    }

    protected boolean canLoseHealth(){
        return this.health > 0 && this.status != Status.DEAD ? true : false;
    }

    public void damageHealth(){        
        if (canLoseHealth()){
            this.health -= this.shieldEquipped == true ? 5.0 : 10.0;
            this.status = Status.DAMAGED;
            if (this.health <= 0){
                this.status = Status.DEAD;
            }
        }
    }

    public boolean getShieldStatus(){
        return shieldEquipped;
    }
    
    public String characterOverall(){
        return this.name;
    }
}