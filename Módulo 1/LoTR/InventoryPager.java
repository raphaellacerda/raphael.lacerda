import java.util.ArrayList;

public class InventoryPager{
    
    private Inventory inventory;
    private int marker;
    
    public InventoryPager(Inventory inventory){
        this.inventory = inventory;
    }
    
    public void skipPage(int marker){
        this.marker = marker > 0 ? marker : 0;
    }
    
    public ArrayList<Item> setLimit(int qty){
        ArrayList<Item> subSet = new ArrayList<>();
        int end = this.marker + qty;
        for (int i = this.marker; i < end && i < this.inventory.getItemList().size(); i++){
            subSet.add(this.inventory.getItem(i));
        }
        return subSet;
    }
    
}
    