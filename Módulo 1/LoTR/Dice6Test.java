

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class Dice6Test{
    
    @Test
    public void create6SidedDice(){
        Dice6 dice = new Dice6();
        assertTrue(dice.checkDice().contains(1));
        assertTrue(dice.checkDice().contains(2));
        assertTrue(dice.checkDice().contains(3));
        assertTrue(dice.checkDice().contains(4));
        assertTrue(dice.checkDice().contains(5));
        assertTrue(dice.checkDice().contains(6));
    }
    
    @Test
    public void throwDiceReturnRandomNumber(){
        Dice6 dice = new Dice6();
        int number1 = dice.throwDice();
        int number2 = number1;
        number1 = dice.throwDice();
        assertFalse(number1 == number2);
    }
    
}