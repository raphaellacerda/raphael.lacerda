    import java.util.ArrayList;
    import static org.junit.Assert.*;
    import org.junit.After;
    import org.junit.Before;
    import org.junit.Test;
    
    public class InventoryTest{
    
    @Test
    public void createInventoryWithDefinedValue(){
        Inventory backpack = new Inventory(50);
        assertEquals(0, backpack.getInventorySize());
    }
    
    @Test
    public void addAnItemToInventory(){
        Inventory backpack = new Inventory(50);
        Item staff = new Item("Staff", 1);
        backpack.addItem(staff);
        assertEquals("Staff", backpack.getDescription(0));
    }
    
    @Test
    public void addTwoItemsToInventory(){
        Inventory backpack = new Inventory(50);
        Item bow = new Item("Bow", 1);
        Item arrow = new Item("Arrow", 10);
        backpack.addItem(bow);
        backpack.addItem(arrow);
        assertEquals("Bow", backpack.getDescription(0));
        assertEquals("Arrow", backpack.getDescription(1));
    }
    
    @Test
    public void getAnNullItemPositionFromInventory(){
        Inventory backpack = new Inventory(50);
        assertNull(backpack.getItem(0));
    }
    
    @Test
    public void addAndRemoveAnItemFromInventory(){
        Inventory backpack = new Inventory(10);
        Item dagger = new Item("Dagger", 1);
        backpack.addItem(dagger);
        backpack.removeItem(0);
        assertNull(backpack.getItem(0));
    }
    
    @Test
    public void addThreeItemsAndRemoveOneFromInventory(){
        Inventory backpack = new Inventory(5);
        Item dagger = new Item("Dagger", 1);
        Item spear = new Item("Spear", 1);
        Item cheese = new Item("Cheese", 1);
        backpack.addItem(dagger);
        backpack.addItem(spear);
        backpack.addItem(cheese);
        backpack.removeItem(1);
        Item result = backpack.getItem(1);
        assertEquals(cheese, result);
    }
    
    @Test
    public void addItemAfterRemoving(){
        Inventory backpack = new Inventory(5);
        Item dagger = new Item("Dagger", 1);
        Item cloak = new Item("Cloak", 1);
        Item cheese = new Item("Cheese", 1);
        backpack.addItem(dagger);
        backpack.addItem(cloak);
        backpack.removeItem(1);
        backpack.addItem(cheese);
        assertEquals(dagger, backpack.getItem(0));
        assertEquals(cheese, backpack.getItem(1));
    }
    
    @Test
    public void getDescriptionFromMultipleItems(){
        Inventory backpack = new Inventory(4);
        Item sword = new Item("Master Sword", 1);
        Item shield = new Item("Hyrulean Shield", 1);
        Item bomb = new Item("Bomb", 1);
        backpack.addItem(sword);
        backpack.addItem(shield);
        backpack.addItem(bomb);
        String itemList = backpack.getItemsDescription();
        assertEquals("Master Sword,Hyrulean Shield,Bomb", itemList);
    }
    
    @Test
    public void getHighestQtyItemFromInventory(){
        Inventory backpack = new Inventory(4);
        Item sword = new Item("Master Sword", 1);
        Item shield = new Item("Hyrulean Shield", 2);
        Item bomb = new Item("Bomb", 3);
        backpack.addItem(sword);
        backpack.addItem(shield);
        backpack.addItem(bomb);
        Item item = backpack.getHigherItemQty();
        assertEquals(bomb, item);
    }
    
    @Test
    public void searchItemInEmptyInventoryByName(){
        Inventory backpack = new Inventory(0);
        Item resultado = backpack.findItem("Staff of Mana");
        assertNull(resultado);
    }
    
    @Test
    public void searchItemInInventory(){
        Inventory backpack = new Inventory(0);
        backpack.addItem(new Item("Staff of Mana", 1));
        Item resultado = backpack.findItem("Staff of Mana");
        assertEquals("Staff of Mana", resultado.getName());
    }
    
    @Test
    public void invertEmptyInventory(){
        Inventory backpack = new Inventory(0);
        assertTrue(backpack.invertItems().isEmpty());
    }
    
    @Test
    public void invertTwoItemsInInventory(){
        Inventory backpack = new Inventory(2);
        Item boomerang = new Item("Boomerang",1);
        Item boots = new Item("Iron Boots",1);
        backpack.addItem(boomerang);
        backpack.addItem(boots);
        ArrayList<Item> inverted = backpack.invertItems();
        assertEquals(boots, inverted.get(0));
        assertEquals(boomerang, inverted.get(1));
    }
    
    @Test
    public void sortEmptyInventory(){
        Inventory inventory = new Inventory(0);
        inventory.sortItems();
        assertTrue(inventory.getItemList().isEmpty());        
    }
}













