import java.util.*;

public class ExemploArrayListSeries{
    public void indicar(){
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Stranger Things");
        lista.add("True Detective");
        lista.add("Mr. Robot");
        lista.add("Punisher");
        lista.add("Dark");
        lista.add("Dexter");
        lista.add("Breaking Bad");
        lista.add("Game of Thrones");
        lista.add("How i met your Mother");
        lista.add("Westworld");
        lista.add("A Casa de Papel");
        lista.add("Black Mirror");
        lista.add("Veep");
        lista.add("Lucifer");
        lista.add("The Last Kingdom");
        lista.add("Two and a Half men");
        
        for (int i = 0; i < lista.size(); i++){
            System.out.println(lista.get(i));
        }
        
        //Checar se está ou não vazia
        boolean taVazia = lista.size() == 0;
        System.out.println(taVazia);
        
        //Remover da lista
        lista.remove(0);
        
        //For in Each
        for (String serie : lista){
            System.out.println(serie);
        }
        
        
    }
    
    
}
