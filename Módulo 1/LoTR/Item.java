public class Item extends Object{
    //quantidade, descriçao
    protected int quantity;
    protected String name;
    
    public Item(String name, int qty){
        this.setName(name);
        this.setQty(qty);
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setQty(int quantity){
        this.quantity = quantity;
    }
    
    public String getName(){
        return this.name;
    }
    
    public int getQty(){
        return this.quantity;
    }
    
    public boolean equals(Object obj){
        Item anotherItem = (Item)obj;
        return this.quantity == anotherItem.getQty() && this.name.equals(anotherItem.getName());
    }
}