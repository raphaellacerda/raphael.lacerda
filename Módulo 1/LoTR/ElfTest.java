import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfTest
{
    @Test
    public void elfBeginsWith100HP(){
        Elf elf = new Elf("Legolas");
        assertEquals(100, elf.getHealth(), .0001);
    }
    
    @Test
    public void shootingArrowShouldLoseArrowGainXp(){
        //Arrange
        Elf anElf = new Elf("Legolas");
        Dwarf aDwarf = new Dwarf("Brok");
        //Act
        anElf.shootArrow(aDwarf);
        //Assert
        assertEquals(1, anElf.getExp());
        assertEquals(1, anElf.getArrow().getQty());
    }
    
    @Test
    public void shootingArrow3TimesShouldLoseArrowGainXp(){
        //Arrange
        Elf anElf = new Elf("Legolas");
        Dwarf aDwarf = new Dwarf("Brok");
        //Act
        anElf.shootArrow(aDwarf);
        anElf.shootArrow(aDwarf);
        anElf.shootArrow(aDwarf);
        //Assert
        assertEquals(2, anElf.getExp());
        assertEquals(0, anElf.getArrow().getQty());
    }
    
    @Test
    public void checkingInitialAmountArrows2(){
        Elf anotherElf = new Elf("Sylvannas");
        assertEquals(2, anotherElf.getArrow().getQty());
    }
    
    @Test
    public void shootingArrowOnDwarfCausesDamage(){
        //Arrange
        Elf anElf = new Elf("Sylvannas");
        Dwarf aDwarf = new Dwarf("Brok");
        //Act
        anElf.shootArrow(aDwarf);
        //Assert
        assertEquals(100.0, aDwarf.getHealth(), .00001);
    }
    
    @Test
    public void elfCreatedMustHaveNewCharStat(){
        Elf anElf = new Elf("Legolas");
        assertEquals(Status.NEW_CHAR, anElf.getStatus());
    }
    
    @Test
    public void elfReceiveItem(){
        Elf elf = new Elf("Sylvannas");
        Item sword = new Item("Sword", 1);
        elf.receiveItem(sword);
        Inventory result = elf.getInventory();
        assertEquals(sword, result.getItem(2));
    }
    
    @Test
    public void elfLoseItem(){
        Elf elf = new Elf("Sylvannas");
        elf.loseItem(elf.getInventory().getItem(0));
        Inventory result = elf.getInventory();
        assertNull(result.getItem(1));
    }
    
    @Test
    public void addElfAndGetCountElf(){
        int result = Elf.totalElfCreated();
        Elf elf = new GreenElf("Legolas");
        int created = elf.totalElfCreated();
        result = (created - result);
        assertEquals(1, result);
    }
    
    @Test
    public void addGreenElfAndGetCountElf(){
        int result = Elf.totalElfCreated();
        GreenElf elf = new GreenElf("Sylvannas");
        int created = elf.totalElfCreated();
        result = (created - result);
        assertEquals(1, result);
    }
    
    @Test
    public void addNightElfAndGetCountElf(){
        int result = Elf.totalElfCreated();
        Elf elf = new NightElf("Sylvannas");
        int created = elf.totalElfCreated();
        result = (created - result);
        assertEquals(1, result);
    }
    
    @Test
    public void addThreeNightElfsAndGetCountElf(){
        int result = Elf.totalElfCreated();
        Elf elf1 = new NightElf("Sylvannas");
        Elf elf2 = new NightElf("Warden");
        Elf elf3 = new NightElf("Tyrande");
        int created = Elf.totalElfCreated();
        result = (created - result);
        assertEquals(3, result);
    }
}


