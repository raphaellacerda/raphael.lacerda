import java.util.HashMap;
import java.util.ArrayList;

public class ElvenArmy{
    
    private HashMap<String, Elf> elvenArmy = new HashMap<>();
    private ArrayList<String> enlistedElves = new ArrayList<>();
      
    public void enlistElf(Elf elf){
        if (canAddElf(elf)){
            elvenArmy.put(elf.getName(), elf);
            this.enlistedElves.add(elf.getName());
        }
    }
    
    private boolean canAddElf(Elf elf){
        String elfClass = elf.getClass().getSimpleName();
        if (elfClass == "NightElf" || elfClass == "GreenElf"){
            return true;
        }
        return false;
    }
    
    public Elf getElf(String name){
        return this.elvenArmy.get(name);
    }
    
    public ArrayList listElvesStatus(Status status){
        ArrayList<Elf> elvenList = new ArrayList<>();
        for (String name : this.enlistedElves){
            Elf elf = this.elvenArmy.get(name);
            if(elf.getStatus() == status);
                elvenList.add(elf);
        }
        return elvenList;
    }
}