import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest{
    
    @Test
    public void dwarfHealthMustBe110(){
        //Arrange
        Dwarf aDwarf = new Dwarf("Gimli");
        //Assert
        assertEquals(110, aDwarf.getHealth(), .000001);        
    }
    
    @Test
    public void dwarfLoses10HP(){
        Dwarf dwarf = new Dwarf("Brok");
        dwarf.damageHealth();
        assertEquals(100, dwarf.getHealth(), .01);
    }
    
     @Test
    public void dwarfLoses10HPTwoTimes(){
        Dwarf dwarf = new Dwarf("Sindri");
        dwarf.damageHealth();
        dwarf.damageHealth();
        assertEquals(90, dwarf.getHealth(), .01);
    }
    
    @Test
    public void dwarfLoses10HPTwelveTimes(){
        Dwarf dwarf = new Dwarf("Sindri");
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        assertEquals(0, dwarf.getHealth(), .01);
    }
    
    @Test
    public void dwarfStatusDeadAfterHealth0(){
        //Arrange
        Dwarf aDwarf = new Dwarf("Brok");
        //Act
        aDwarf.damageHealth();
        aDwarf.damageHealth();
        aDwarf.damageHealth();
        aDwarf.damageHealth();
        aDwarf.damageHealth();
        aDwarf.damageHealth();
        aDwarf.damageHealth();
        aDwarf.damageHealth();
        aDwarf.damageHealth();
        aDwarf.damageHealth();
        aDwarf.damageHealth();
        
        //Assert
        assertEquals(Status.DEAD, aDwarf.getStatus());
        
    }
    
    @Test
    public void dwarfDontTakeDamageAfterDead(){
        //Arrange
        Dwarf dwarf = new Dwarf("Sindri");
        //Act
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        dwarf.damageHealth();
        //Assert
        assertEquals(0, dwarf.getHealth(), .00001);
    }
    
    @Test
    public void dwarfStatusDamagedAfterDamage(){
        //Arrange
        Dwarf aDwarf = new Dwarf("Brok");
        //Act
        aDwarf.damageHealth();
        //Assert
        assertEquals(Status.DAMAGED, aDwarf.getStatus());
        
    }
    
    @Test
    public void dwarfEquipShield(){
        Dwarf dwarf = new Dwarf("Sindri");
        dwarf.equipShield();
        assertTrue(dwarf.getShieldStatus());
        
    }
    
    @Test
    public void dwarfTakesHalfDamageEquippingShield(){
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.equipShield();
        dwarf.damageHealth();
        assertEquals(105, dwarf.getHealth(), 1e-9);
    }
    
    @Test
    public void dwarfReceiveItem(){
        Dwarf dwarf = new Dwarf("Brok");
        Item hammer = new Item("Hammer", 1);
        dwarf.receiveItem(hammer);
        Inventory result = dwarf.getInventory();
        assertEquals(hammer, result.getItem(1));
    }
    
    @Test
    public void dwarfReceiveAndLoseItem(){
        Dwarf dwarf = new Dwarf("Brok");
        Item hammer = new Item("Hammer", 1);
        dwarf.receiveItem(hammer);
        dwarf.loseItem(hammer);
        Inventory result = dwarf.getInventory();
        assertNull(result.getItem(1));
    }
    
    @Test
    public void dwarfReceiveTwoItems(){
        Dwarf dwarf = new Dwarf("Brok");
        Item hammer = new Item("Hammer", 1);
        Item lantern = new Item("Lantern", 1);
        dwarf.receiveItem(hammer);
        dwarf.receiveItem(lantern);
        Inventory result = dwarf.getInventory();
        assertEquals(hammer, result.getItem(1));
        assertEquals(lantern, result.getItem(2));
    }
    
    @Test
    public void dwarfReceiveTwoItemsThenLosesFirstReceived(){
        Dwarf dwarf = new Dwarf("Brok");
        Item hammer = new Item("Hammer", 1);
        Item lantern = new Item("Lantern", 1);
        dwarf.receiveItem(hammer);
        dwarf.receiveItem(lantern);
        dwarf.loseItem(hammer);
        Inventory result = dwarf.getInventory();
        assertEquals(lantern, result.getItem(1));
        assertNull(result.getItem(2));
    }
}